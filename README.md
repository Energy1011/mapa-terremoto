# Mapa-terremoto
México 19/09/2017
### Publico general
Este mapa pretende ser una herramienta de consulta para localizar los centros de acopio por estados, (mucha gente no ubica facilmente los centros de acopio más cercanos en sus ciudades para llevar víveres, herramientas o para ofrecer servicios voluntarios, este mapa pretende facilitar la ubicación) la información aquí mostrada es información obtenida por gente voluntaria, es una iniciativa libre no oficial a alguna dependencia de gobierno o empresa, esta herramienta pretende ser útil e informativa.

Si deseas contribuir a agregar más sitios, puedes hacerlo también desde el google form que se encuentra dentro del mapa en el apartado de información general.

### Developers
Este mapa pretende mostrar los centros de acopio por ciudades, el código del mapa es simple, puedes colaborar agregando la información de los centros de acopio de tu ciudad en código duro en el archivo index.html (var app.marks), no utiliza base de datos.

### Enlace al mapa
Este mapa se encuentra hosteado en www.softnaya.com/mapaterremoto por el momento.

## Licencia
Este mapa es software libre bajo la licencia GPLv3.
